package server.query

import com.xenomachina.argparser.SystemExitException
import server.data.QueryResult
import xmlwise.Plist
import java.io.File
import java.lang.IllegalArgumentException
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLException

/**
 * An abstract representation of a DocSet in the filesystem. It's properties
 * are backed by the files in the filesystem DocSet.
 * @constructor [docSet] A [File] representing the DocSet. If [docSet] is invalid, throw [IllegalArgumentException]
 * @property [pListFile]
 * @property [index]
 * @property [connection]
 * @property [properties]
 * @property keyword
 */
class DocSet(private val docSet: File) {

    private val pListFile: File = File(this.docSet, "Contents/Info.plist")
    private val index: File = File(this.docSet, "Contents/Resources/docSet.dsidx")

    /**
     * Initialization checks to see if this DocSet is valid.
     * If it isn't throw an IllegalArgumentException that details the error
     */
    init {
        require(this.docSet.extension == "docset") { "${this.docSet.name} is not a DocSet." }
        require(this.docSet.exists()) { "${this.docSet.name} does not exist." }
        require(this.docSet.isDirectory) { "${this.docSet.name} is not a directory." }
        require(this.pListFile.exists()) { "${this.docSet.name} does not have a valid Info.plist file." }
        require(this.index.exists()) { "${this.docSet.name} is not indexed in a docSet.dsidx file." }
    }

    /**
     * Loads the [pListFile] from disk on every access
     * to ensure that this variable contains accurate information
     * stored in the file
     */
    private val properties: MutableMap<String, Any>
        get() = Plist.load(pListFile)

    /**
     * Use [properties] as the backing for the keyword to ensure
     * the search and storage of the [keyword] is always consistent
     * with information on disk.
     */
    var keyword: String
        get() = properties["CFBundleIdentifier"] as String
        /**
         * @param [value] the value to set this DocSet keyword to.
         */
        set(value) = with(properties) {
            this["CFBundleIdentifier"] = value
            Plist.store(this, pListFile)
        }

    /**
     * Use [properties] as the backing for the DocSet [name].
     * This is the full name of the DocSet, eg. `Python 3`.
     */
    val name: String
        get() = properties["CFBundleName"] as String

    /**
     * Establishes a connection to the SQLite [index].
     * On connection, it also ensure a view `searchIndex` exists.
     * If the view doesn't exist, create it.
     */
    private val connection: Connection
        get() = try {
            DriverManager.getConnection("jdbc:sqlite:$index").also {
                it.createStatement().execute(
                        """
                        CREATE VIEW IF NOT EXISTS searchIndex AS
                        SELECT ZTOKENNAME AS name, ZTYPENAME AS type, ZPATH AS path
                        FROM ZTOKEN
                        INNER JOIN ZTOKENMETAINFORMATION ON ZTOKEN.ZMETAINFORMATION = ZTOKENMETAINFORMATION.Z_PK
                        INNER JOIN ZFILEPATH ON ZTOKENMETAINFORMATION.ZFILE = ZFILEPATH.Z_PK
                        INNER JOIN ZTOKENTYPE ON ZTOKEN.ZTOKENTYPE = ZTOKENTYPE.Z_PK;
                    """
                )
            }
        } catch (e: SQLException) {
            throw SystemExitException("SQL Error: ${e.message}", 3)
        }


    /**
     * This function acquires a single use connection, attempts the SQL query, and closes the connection
     * @throws [SystemExitException] in the event of a SQL error
     * @param name the name to search for in the SQLite index
     */
    fun search(name: String) = this.connection.use {
        try {
            it.prepareStatement("SELECT * FROM searchIndex WHERE name LIKE ?").apply {
                setString(1, "%$name%")
            }.executeQuery().map { result ->
                QueryResult(
                        name = result.getString("name"),
                        path = result.getString("path"),
                        type = result.getString("type")
                )
            }
        } catch (e: SQLException) {
            throw SystemExitException("SQL Error: ${e.message}", 3)
        }
    }
}

inline fun <T> ResultSet.map(transform: (ResultSet) -> T): List<T> {
    val result = ArrayList<T>()
    while (this.next()) {
        result.add(transform(this))
    }
    return result
}

/**
 * @return A list of valid DocSets that are children of [this] File
 */
fun File.findValidDocSets() = this.walk().map {
    try {
        DocSet(it)
    } catch (e: IllegalArgumentException) {
        null
    }
}.filterNotNull()