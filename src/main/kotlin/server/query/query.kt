package server.query

import java.io.File

fun updateDocSet(updatedKeyword: String, docSet: String, docSetStore: String): Map<String, String> = with(DocSet(File(docSetStore, docSet))) {
    this.keyword = updatedKeyword
    return mapOf("name" to this.name, "keyword" to this.keyword)
}

fun queryDocSets(query: String, keywords: List<String>, docSetStore: String) = File(docSetStore).findValidDocSets().filter {
    keywords.isEmpty() || it.keyword in keywords
}.map {
    it.search(query)
}.flatten().toList()