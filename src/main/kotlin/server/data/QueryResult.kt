package server.data

data class QueryResult(
        val name: String,
        val type: String,
        val path: String
)