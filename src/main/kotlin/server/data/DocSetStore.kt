package server.data

import java.io.File
import java.util.*

object DocSetStore {

    val docSetStore: String = with(Properties()) {
        this.load(
                Thread.currentThread().contextClassLoader.getResourceAsStream("application.properties")
        )

        // get the application.properties DocSet path. If it's not present, default for a hidden home folder,
        // and make those folders as well.
        this["docset-store.path"] as String? ?: ("${System.getProperty("user.home")}/.docsets/".also {
            File(it).mkdirs()
        })
    }

}