package server

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import server.data.DocSetStore
import server.parse.OutputFormat
import server.parse.ParsedArgs
import server.parse.toJson
import server.query.findValidDocSets
import server.query.queryDocSets
import server.query.updateDocSet
import java.io.File

fun main(args: Array<String>): Unit = mainBody {
    ArgParser(args).parseInto(::ParsedArgs).run {
        processKeywords(availableKeywords)?.run {
            prettyPrint(this, outputFormat, prettyPrint)
        }
        processQuery(query, keywords)?.run {
            prettyPrint(this, outputFormat, prettyPrint)
        }
        processUpdate(updatedKeyword, updatedDocSet)?.run {
            prettyPrint(this, outputFormat, prettyPrint)
        }
    }
}

fun prettyPrint(value: Any, outputFormat: OutputFormat, pretty: Boolean): Unit = when (outputFormat) {
    OutputFormat.JSON -> print(value.toJson(pretty))
}

fun processKeywords(flag: Boolean) = if (flag) {
    File(DocSetStore.docSetStore).findValidDocSets().map {
        mapOf("name" to it.name, "keyword" to it.keyword)
    }.toList()
} else {
    null
}

fun processQuery(query: String, keywords: List<String>) = if (!query.isBlank()) {
    queryDocSets(query.trim(), keywords.map { it.trim() }, DocSetStore.docSetStore)
} else {
    null
}

fun processUpdate(keyword: String, docSet: String) = if (!(keyword + docSet).isBlank()) {
    updateDocSet(keyword.trim(), docSet.trim(), DocSetStore.docSetStore)
} else {
    null
}


