package server.parse

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

fun Any?.toJson(pretty: Boolean = false) = when(pretty) {
    true -> jacksonObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this)
    false -> jacksonObjectMapper().writeValueAsString(this)
}