package server.parse

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.SystemExitException
import com.xenomachina.argparser.default
import java.io.File

class ParsedArgs(parser: ArgParser) {

    val query: String by parser.storing(
            "-q", "--queryDocSets",
            help = "The search query."
    ).default("").addValidator {
        if (!this.value.isBlank() && !updatedKeyword.isBlank() && !updatedDocSet.isBlank()) {
            throw SystemExitException("You must queryDocSets and updated DocSets independently", 1)
        }
    }

    val keywords: List<String> by parser.adding(
             "-k", "--keyword",
            help = "The keywords to use when searching DocSets"
    )

    val outputFormat: OutputFormat by parser.mapping(
            "--json" to OutputFormat.JSON,
            help = "The output format to specify"
    ).default(OutputFormat.JSON)

    val prettyPrint: Boolean by parser.flagging(
            "-p", "--pretty-print",
            help = "Pretty print the output to STDOUT"
    ).default(false)


    val updatedKeyword: String by parser.storing(
            "-u", "--update-keyword",
            help = "The updated search keyword to apply"
    ).default("").addValidator {
        if (!this.value.isBlank() && updatedDocSet.isBlank()) {
            throw SystemExitException("You must include a DocSet to update.", 1)
        }
    }

    val updatedDocSet: String by parser.storing(
            "-d", "--update-docset",
            help = "The DocSet to apply to updated search keyword to"
    ).default("").addValidator {
        if (!this.value.isBlank() && updatedKeyword.isBlank()) {
            throw SystemExitException("You must include the updated search keyword.", 1)
        }

        if (!this.value.isBlank() && File(this.value).extension != "docset") {
            throw SystemExitException("${this.value} is not a valid DocSet path.", 1)
        }
    }

    val availableKeywords by parser.flagging(
            "-a", "--available-keywords",
            help = "Get a list of searchable keywords."
    ).default(false).addValidator {
        if (this.value && (!query.isBlank() || !keywords.isEmpty() || !updatedDocSet.isBlank() || !updatedKeyword.isBlank())) {
            throw SystemExitException("You must list keywords independently of all other operations", 1)
        }
    }

}

enum class OutputFormat {
    JSON
}