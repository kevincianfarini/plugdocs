package server.integration

import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import server.data.DocSetStore
import server.query.DocSet
import server.query.findValidDocSets
import java.io.File
import java.lang.IllegalArgumentException
import java.sql.DriverManager

class TestDocSetIntegration {

    @get:Rule
    var thrown: ExpectedException = ExpectedException.none()

    @Test
    fun testFindValidDocSetsExtension() {
        // arrange
        val docSetStore = File(DocSetStore.docSetStore)

        // act
        val docSets = docSetStore.findValidDocSets()

        //assert
        assertEquals(4, docSets.count())
    }

    @Test
    fun testValidDocSet() {
        // arrange
        val valid = File(DocSetStore.docSetStore, "kotlin.docset")

        // act
        DocSet(valid)

        // assert: No exception being thrown in implicit passing test
    }

    @Test
    fun testInvalidDocSetNameException() {
        // arrange
        val invalid = File(DocSetStore.docSetStore, "invalid.notdocset")

        // assert
        thrown.expect(IllegalArgumentException::class.java)
        thrown.expectMessage("invalid.notdocset is not a DocSet.")

        //act
        DocSet(invalid)

    }

    @Test
    fun testInvalidDocSetDirectoryException() {
        // arrange
        val invalid = File(DocSetStore.docSetStore, "invalidfile.docset")

        //assert
        thrown.expect(IllegalArgumentException::class.java)
        thrown.expectMessage("invalidfile.docset is not a directory.")

        //act
        DocSet(invalid)
    }

    @Test
    fun testInvalidDocSetDoesNotExist() {
        // arrange
        val invalid = File(DocSetStore.docSetStore, "blah-invalid-no-plist.docset")

        // assert
        thrown.expect(IllegalArgumentException::class.java)
        thrown.expectMessage("blah-invalid-no-plist.docset does not exist.")

        //act
        DocSet(invalid)

    }

    @Test
    fun testInvalidDocSetNoPlistFile() {
        // arrange
        val invalid = File(DocSetStore.docSetStore, "invalid-no-plist.docset")

        //assert
        thrown.expect(IllegalArgumentException::class.java)
        thrown.expectMessage("invalid-no-plist.docset does not have a valid Info.plist file.")


        //act
        DocSet(invalid)
    }

    @Test
    fun testInvalidDocSetNoIndexFile() {
        // arrange
        val invalid = File(DocSetStore.docSetStore, "invalid-no-index.docset")

        //assert
        thrown.expect(IllegalArgumentException::class.java)
        thrown.expectMessage("invalid-no-index.docset is not indexed in a docSet.dsidx file.")


        //act
        DocSet(invalid)
    }

    @Test
    fun testKeyWordGet() {
        // arrange
        val docSet = DocSet(File(DocSetStore.docSetStore, "kotlin.docset"))

        // act
        val keyword = docSet.keyword

        // assert
        assertEquals("kotlin", keyword)
    }

    @Test
    fun testKeyWordSet() {
        // arrange
        val docSet = DocSet(File(DocSetStore.docSetStore, "kotlin.docset"))

        // act
        assertEquals("kotlin", docSet.keyword)
        docSet.keyword = "kdoc"

        // assert
        assertEquals("kdoc", docSet.keyword)

        // cleanup
        docSet.keyword = "kotlin"
    }

    @Test
    fun testSearchSimple() {
        // arrange
        val docSet = DocSet(File(DocSetStore.docSetStore, "kotlin.docset"))

        // act
        val results = docSet.search("File.walk")

        // assert
        assertEquals(3, results.size)
    }

    @Test
    fun testSearchNoCase() {
        // arrange
        val docSet = DocSet(File(DocSetStore.docSetStore, "kotlin.docset"))

        // act
        val results = docSet.search("FiLE.WaLk")

        // assert
        assertEquals(3, results.size)
    }

    @Test
    fun testGettingConnectionMakesSearchIndexView() {
        // arrange
        val conn = DriverManager.getConnection("jdbc:sqlite:${File(DocSetStore.docSetStore, "Python3.docset/Contents/Resources/docSet.dsidx").absolutePath}")
        conn.createStatement().execute("DROP VIEW IF EXISTS searchIndex;")
        val docSet = DocSet(File(DocSetStore.docSetStore, "Python3.docset"))

        // act
        docSet.search("os.walk")

        // assert
        conn.createStatement().executeQuery("SELECT name FROM sqlite_master WHERE type='view' UNION ALL SELECT name FROM sqlite_temp_master WHERE type='view';").also {
            assertEquals("searchIndex", it.getString("name"))
        }
    }
}